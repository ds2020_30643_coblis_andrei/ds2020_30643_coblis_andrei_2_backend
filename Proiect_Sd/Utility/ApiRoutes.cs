﻿namespace Proiect_Sd.Utility
{
    public static class ApiRoutes
    {
        public const string Root = "api";

        public const string Version = "v1";

        public const string Base = Root + "/" + Version;

        public static class Names
        {
            public const string GetUser = "GetUser";

            public const string GetPatient = "GetPatient";

            public const string GetCaregiver = "GetCaregiver";

            public const string GetMedication = "GetMedication";

            public const string GetDoctor = "GetDoctor";

            public const string GetMedicationPlan = "GetMedicationPlan";
        }

        public static class Identity
        {
            public const string Login = Base + "/login";
        }

        public static class Users
        {
            public const string GetAll = Base + "/users";

            public const string Get = Base + "/users/{userId}";

            public const string Create = Base + "/users";

            public const string Update = Base + "/users/{userId}";

            public const string Delete = Base + "/users/{userId}";
        }

        public static class Patients
        {
            public const string GetAll = Base + "/patients";

            public const string Get = Base + "/patients/{patientId}";

            public const string Create = Base + "/patients";

            public const string Update = Base + "/patients/{patientId}";

            public const string Delete = Base + "/patients/{patientId}";
        }

        public static class Caregivers
        {
            public const string GetAll = Base + "/caregivers";

            public const string Get = Base + "/caregivers/{caregiverId}";

            public const string Create = Base + "/caregivers";

            public const string Update = Base + "/caregivers/{caregiverId}";

            public const string Delete = Base + "/caregivers/{caregiverId}";
        }

        public static class Medications
        {
            public const string GetAll = Base + "/medications";

            public const string Get = Base + "/medications/{medicationId}";

            public const string Create = Base + "/medications";

            public const string Update = Base + "/medications/{medicationId}";

            public const string Delete = Base + "/medications/{medicationId}";
        }

        public static class Doctors
        {
            public const string GetAll = Base + "/doctors";

            public const string Get = Base + "/doctors/{doctorId}";

            public const string Create = Base + "/doctors";

            public const string Update = Base + "/doctors/{doctorId}";

            public const string Delete = Base + "/doctors/{doctorId}";
        }

        public static class MedicationPlans
        {
            public const string GetAll = Base + "/medicationPlans";

            public const string Get = Base + "/medicationPlans/{medicationPlanId}";

            public const string Create = Base + "/medicationPlans";

            public const string Update = Base + "/medicationPlans/{medicationPlanId}";

            public const string Delete = Base + "/medicationPlans/{medicationPlanId}";
        }
    }
}
