﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.Repositories.Interfaces;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAuthenticationRepository _authenticationRepository;

        public AuthenticationService(IAuthenticationRepository authenticationRepository)
        {
            _authenticationRepository = authenticationRepository;
        }

        public async Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO)
        {
            return await _authenticationRepository.CheckIfLoginCredentialsExist(loginRequestDTO);
        }

        public async Task<string> GetUserRoleAsync(LoginRequestDTO loginRequestDTO)
        {
            return await _authenticationRepository.GetUserRole(loginRequestDTO);
        }
    }
}
