﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Proiect_Sd.Hubs;
using Proiect_Sd.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Proiect_Sd.RabbitMq
{
    public class MessageReceiver : BackgroundService
    {
        private IModel _channel;
        private MessageHub _hub;
        private IConnection _connection;
        private readonly string _hostname;
        private readonly string _queueName;
        private readonly string _username;
        private readonly string _password;

        public MessageReceiver(IOptions<RabbitMqConfiguration> rabbitMqOptions, MessageHub hub)
        {
            _hostname = rabbitMqOptions.Value.Hostname;
            _queueName = rabbitMqOptions.Value.QueueName;
            _username = rabbitMqOptions.Value.UserName;
            _password = rabbitMqOptions.Value.Password;
            _hub = hub;
            InitializeRabbitMqListener();
        }

        private void InitializeRabbitMqListener()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);

        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var response = JsonConvert.DeserializeObject(content);

                HandleMessage(response);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(_queueName, false, consumer);

            return Task.CompletedTask;
        }

        private async void HandleMessage(Object response)
        {
            // ... check the rules
            /*
             * R1: Sleep period > 7 hours
             * R2: Leaving activity > 5 hours
             * R3: Bathroom time > 30 mins
             * ==> Send alert
             */

            var responseString = response.ToString();
            var split = responseString.Split(',');

            var patientId = split[0].Substring(19, 1);
            var start = Convert.ToInt64(split[1].Substring(13, 13));
            var end = Convert.ToInt64(split[2].Substring(11, 13));
            var activity = split[3].Substring(17);
            activity = activity.Substring(0, activity.Length - 4);

            var activityDuration = (DateTimeOffset.FromUnixTimeMilliseconds(end).UtcDateTime) - (DateTimeOffset.FromUnixTimeMilliseconds(start).UtcDateTime);
            var message = $"Patient {patientId} has problems with {activity} activity";

            switch(activity)
            {
                case "Sleeping":
                    if(activityDuration > TimeSpan.FromHours(7))
                        await _hub.SendMessage(message);
                    break;
                case "Leaving":
                    if(activityDuration > TimeSpan.FromHours(5))
                        await _hub.SendMessage(message);
                    break;
                case "Toileting":
                    if (activityDuration > TimeSpan.FromMinutes(30))
                        await _hub.SendMessage(message);
                    break;
                case "Grooming":
                    if (activityDuration > TimeSpan.FromMinutes(30))
                        await _hub.SendMessage(message);
                    break;
                case "Showering":
                    if (activityDuration > TimeSpan.FromMinutes(30))
                        await _hub.SendMessage(message);
                    break;
            }

            Console.WriteLine(response.ToString());

            //if(_hub.Clients != null)
            //{
            //    
            //}
        }
    }
}
