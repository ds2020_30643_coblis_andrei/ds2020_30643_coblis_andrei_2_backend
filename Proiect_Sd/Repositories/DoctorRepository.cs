﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Sd.Repositories
{
    public class DoctorRepository : IDoctorRepository
    {
        private readonly DataContext _context;

        public DoctorRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfDoctorExists(int doctorId)
        {
            using(_context)
            {
                return await _context.Doctors.AnyAsync(d => d.DoctorId == doctorId);
            }
        }

        public async Task<Doctor> CreateDoctor(Doctor doctor)
        {
            using(_context)
            {
                _context.Doctors.Add(doctor);
                await _context.SaveChangesAsync();

                return doctor;
            }
        }

        public async Task<bool> DeleteDoctor(Doctor doctor)
        {
            using(_context)
            {
                _context.Doctors.Remove(doctor);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<Doctor> GetDoctorById(int doctorId)
        {
            using(_context)
            {
                return await _context.Doctors.FirstOrDefaultAsync(d => d.DoctorId == doctorId);
            }
        }

        public async Task<DoctorResponseDTO> GetDoctorResponseById(int doctorId)
        {
            using (_context)
            {
                var doctors = _context.Doctors;
                var users = _context.Users;

                var doctorResponseDTO = from doctor in doctors
                                            join user in users
                                            on doctor.UserId equals user.UserId
                                            where doctor.DoctorId == doctorId
                                            select new DoctorResponseDTO()
                                            {
                                                Id = doctor.DoctorId,
                                                Username = user.Username,
                                                UserId = user.UserId
                                            };

                return await doctorResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<DoctorResponseDTO>> GetDoctors()
        {
            using(_context)
            {
                var doctors = _context.Doctors;
                var users = _context.Users;

                var doctorResponseDTOList = from doctor in doctors
                                            join user in users
                                            on doctor.UserId equals user.UserId
                                            select new DoctorResponseDTO()
                                            {
                                                Id = doctor.DoctorId,
                                                Username = user.Username,
                                                UserId = user.UserId
                                            };

                return await doctorResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdateDoctor(Doctor doctor)
        {
            using(_context)
            {
                _context.Doctors.Update(doctor);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
