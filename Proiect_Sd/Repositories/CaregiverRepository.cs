﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Sd.Repositories
{
    public class CaregiverRepository : ICaregiverRepository
    {
        private readonly DataContext _context;

        public CaregiverRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfCaregiverExists(int caregiverId)
        {
            using(_context)
            {
               return await _context.Caregivers.AnyAsync(c => c.CaregiverId == caregiverId);
            }
        }

        public async Task<Caregiver> CreateCaregiver(Caregiver caregiver)
        {
            using (_context)
            {
                _context.Caregivers.Add(caregiver);
                await _context.SaveChangesAsync();

                return caregiver;
            }
        }

        public async Task<bool> DeleteCaregiver(Caregiver caregiver)
        {
            using(_context)
            {
                _context.Caregivers.Remove(caregiver);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<Caregiver> GetCaregiverById(int caregiverId)
        {
            using (_context)
            {
                return await _context.Caregivers.FirstOrDefaultAsync(c => c.CaregiverId == caregiverId);
            }
        }

        public async Task<CaregiverResponseDTO> GetCaregiverResponseById(int caregiverId)
        {
            using (_context)
            {
                var caregivers = _context.Caregivers;
                var users = _context.Users;

                var caregiverResponseDTO = from caregiver in caregivers
                                               join user in users
                                               on caregiver.UserId equals user.UserId
                                               where caregiver.CaregiverId == caregiverId
                                               select new CaregiverResponseDTO()
                                               {
                                                   Id = caregiver.CaregiverId,
                                                   Username = user.Username,
                                                   UserId = user.UserId
                                               };

                return await caregiverResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<CaregiverResponseDTO>> GetCaregivers()
        {
            using (_context)
            {
                var caregivers = _context.Caregivers;
                var users = _context.Users;

                var caregiverResponseDTOList = from caregiver in caregivers
                                               join user in users
                                               on caregiver.UserId equals user.UserId
                                               select new CaregiverResponseDTO()
                                               {
                                                   Id = caregiver.CaregiverId,
                                                   Username = user.Username,
                                                   UserId = user.UserId
                                               };

                return await caregiverResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdateCaregiver(Caregiver caregiver)
        {
            using(_context)
            {
                _context.Caregivers.Update(caregiver);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
