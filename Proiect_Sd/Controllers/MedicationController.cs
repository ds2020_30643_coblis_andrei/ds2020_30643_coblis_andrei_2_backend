﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Services.Interfaces;
using Proiect_Sd.Utility;
using System.Threading.Tasks;

namespace Proiect_Sd.Controllers
{
    [ApiController]
    public class MedicationController : ControllerBase
    {
        private readonly IMedicationService _medicationService;

        public MedicationController(IMedicationService medicationService)
        {
            _medicationService = medicationService;
        }

        [ProducesResponseType(typeof(MedicationResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Medications.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var medications = await _medicationService.GetMedications();

            return Ok(medications);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MedicationResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Medications.Get, Name = ApiRoutes.Names.GetMedication)]
        public async Task<IActionResult> Get([FromRoute] int medicationId)
        {
            var medication = await _medicationService.GetMedicationResponseByIdAsync(medicationId);

            if(medication ==  null)
            {
                return NotFound();
            }

            return Ok(medication);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Medications.Create)]
        public async Task<IActionResult> Create([FromBody] MedicationRequestDTO medicationRequestDTO)
        {
            var medication = await _medicationService.CreateMedicationAsync(medicationRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetMedication, new { medicationId = medication.MedicationId }, "New medication created");
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.Medications.Update)]
        public async Task<IActionResult> Update([FromRoute] int medicationId, [FromBody] MedicationRequestDTO medicationRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _medicationService.CheckIfMedicationExistsAsync(medicationId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _medicationService.UpdateMedicationAsync(medicationRequestDTO, medicationId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Medications.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int medicationId)
        {
            var exists = await _medicationService.CheckIfMedicationExistsAsync(medicationId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _medicationService.DeleteMedicationAsync(medicationId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
