﻿namespace Proiect_Sd.DTOs.Response
{
    public class PatientResponseDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int CaregiverId { get; set; }

        public string Username { get; set; }

        public string CaregiverName { get; set; }

        public string MedicalRecord { get; set; }

        public string Activity { get; set; }

        public long StartTime { get; set; }

        public long EndTime { get; set; }
    }
}
