﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using Proiect_Sd.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public class PatientService : IPatientService
    {
        private readonly IPatientRepository _patientRepository;
        private readonly IMapper _mapper;

        public PatientService(IPatientRepository patientRepository, IMapper mapper)
        {
            _patientRepository = patientRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfPatientExistsAsync(int patientId)
        {
            return await _patientRepository.CheckIfPatientExists(patientId);
        }

        public async Task<Patient> CreatePatientAsync(PatientRequestDTO patientRequestDTO)
        {
            var patient = _mapper.Map<Patient>(patientRequestDTO);
            return await _patientRepository.CreatePatient(patient);
        }

        public async Task<bool> DeletePatientAsync(int patientId)
        {
            var patient = await GetPatientByIdAsync(patientId);

            return await _patientRepository.DeletePatient(patient);
        }

        public async Task<Patient> GetPatientByIdAsync(int patientId)
        {
            return await _patientRepository.GetPatientById(patientId);
        }

        public async Task<PatientResponseDTO> GetPatientResponseByIdAsync(int patientId)
        {
            return await _patientRepository.GetPatientResponseById(patientId);
        }

        public async Task<List<PatientResponseDTO>> GetPatients()
        {
            return await _patientRepository.GetPatients();
        }

        public async Task<bool> UpdatePatientAsync(PatientRequestDTO patientRequestDTO, int patientId)
        {
            var patient = _mapper.Map<Patient>(patientRequestDTO);
            patient.PatientId = patientId;

            return await _patientRepository.UpdatePatient(patient);
        }
    }
}
