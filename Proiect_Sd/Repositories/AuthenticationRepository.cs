﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.Repositories.Interfaces;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly DataContext _context;

        public AuthenticationRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO)
        {
            using(_context)
            {
                return await _context.Users.AnyAsync(u => u.Username == loginRequestDTO.Username && u.Password == loginRequestDTO.Password);
            }
        }

        public async Task<string> GetUserRole(LoginRequestDTO loginRequestDTO)
        {
            using(_context)
            {
                var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == loginRequestDTO.Username && u.Password == loginRequestDTO.Password);
                return user.Role;
            }
        }
    }
}
