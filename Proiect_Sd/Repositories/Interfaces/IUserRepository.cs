﻿using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<List<User>> GetUsers();
        Task<User> GetUserById(int userId);
        Task<User> CreateUser(User user);
        Task<bool> UpdateUser(User user);
        Task<bool> CheckIfUserExists(int userId);
        Task<bool> DeleteUser(User user);
    }
}
