﻿using Proiect_Sd.DTOs.Request;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public interface IAuthenticationService
    {
        Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO);
        Task<string> GetUserRoleAsync(LoginRequestDTO loginRequestDTO);
    }
}
