﻿using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IDoctorRepository
    {
        Task<List<DoctorResponseDTO>> GetDoctors();
        Task<DoctorResponseDTO> GetDoctorResponseById(int doctorId);
        Task<Doctor> GetDoctorById(int doctorId);
        Task<Doctor> CreateDoctor(Doctor doctor);
        Task<bool> UpdateDoctor(Doctor doctor);
        Task<bool> CheckIfDoctorExists(int doctorId);
        Task<bool> DeleteDoctor(Doctor doctor);
    }
}
