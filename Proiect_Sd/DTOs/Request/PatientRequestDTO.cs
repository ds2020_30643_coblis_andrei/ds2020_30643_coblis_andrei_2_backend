﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.DTOs.Request
{
    public class PatientRequestDTO
    {
        [Required(ErrorMessage = "Please enter an user ID")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter a caregiver ID")]
        public int CaregiverId { get; set; }

        [Required(ErrorMessage = "Please enter a medical record")]
        [StringLength(100, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string MedicalRecord { get; set; }
    }
}
