﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Services.Interfaces;
using Proiect_Sd.Utility;
using System.Threading.Tasks;

namespace Proiect_Sd.Controllers
{
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [ProducesResponseType(typeof(PatientResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Patients.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var patients = await _patientService.GetPatients();

            return Ok(patients);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(PatientResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Patients.Get, Name = ApiRoutes.Names.GetPatient)]
        public async Task<IActionResult> Get([FromRoute] int patientId)
        {
            var patient = await _patientService.GetPatientResponseByIdAsync(patientId);

            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Patients.Create)]
        public async Task<IActionResult> Create([FromBody] PatientRequestDTO patientRequestDTO)
        {
            var patient = await _patientService.CreatePatientAsync(patientRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetPatient, new { patientId = patient.PatientId }, "New patient created");
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.Patients.Update)]
        public async Task<IActionResult> Update([FromRoute] int patientId, [FromBody] PatientRequestDTO patientRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _patientService.CheckIfPatientExistsAsync(patientId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _patientService.UpdatePatientAsync(patientRequestDTO, patientId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Patients.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int patientId)
        {
            var exists = await _patientService.CheckIfPatientExistsAsync(patientId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _patientService.DeletePatientAsync(patientId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
