﻿using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface ICaregiverRepository
    {
        Task<List<CaregiverResponseDTO>> GetCaregivers();
        Task<CaregiverResponseDTO> GetCaregiverResponseById(int caregiverId);
        Task<Caregiver> GetCaregiverById(int caregiverId);
        Task<Caregiver> CreateCaregiver(Caregiver caregiver);
        Task<bool> UpdateCaregiver(Caregiver caregiver);
        Task<bool> CheckIfCaregiverExists(int caregiverId);
        Task<bool> DeleteCaregiver(Caregiver caregiver);
    }
}
